import scrapy
import logging
from scrapy.loader import ItemLoader
from scrapy.http import FormRequest
#from ..items import QuotetutorialItem
#from ..items import diffDayNameEnglish
from datetime import datetime
from datetime import timedelta
from ..connector import GetNeo4j
#from ..items import parse_date





class DateTimeSpider(scrapy.Spider):

    name = "datetime"


    def __init__(self, *args, **kwargs):
        logger = logging.getLogger('scrapy.middleware')
        logger.setLevel(logging.WARNING)
        super().__init__(*args,**kwargs)
        print("Hello")
        if 'page' not in kwargs:
            raise AttributeError('You need to provide a valid page name to crawl!'
                                 'scrapy fb -a page="PAGENAME"')
        elif self.page.find('https://www.facebook.com/') != -1:
            self.page = self.page[25:]
            self.logger.info('Page attribute provided, scraping "{}"'.format(self.page))
        elif self.page.find('https://mbasic.facebook.com/') != -1:
            self.page = self.page[28:]
            self.logger.info('Page attribute provided, scraping "{}"'.format(self.page))
        elif self.page.find('https://m.facebook.com/') != -1:
            self.page = self.page[23:]
            self.logger.info('Page attribute provided, scraping "{}"'.format(self.page))
        else:
            self.logger.info('Page attribute provided, scraping "{}"'.format(self.page))
        
        #parse year
        if 'year' not in kwargs:
            self.year = 2018
            self.logger.info('Year attribute not found, set scraping back to {}'.format(self.year))
        else:
            assert int(self.year) <= 2019 and int(self.year) >= 2006
            'Year must be an int number 2006 <= year <= 2019'
            self.year = int(self.year)    #arguments are passed as strings
            self.logger.info('Year attribute found, set scraping back to {}'.format(self.year))

        #parse lang, if not provided (but is supported) it will be guessed in parse_home
        if 'lang' not in kwargs:
            self.logger.info('Language attribute not provided, I will try to guess it from the fb interface')
            self.logger.info('To specify, add the lang parameter: scrapy fb -a lang="LANGUAGE"')
            self.logger.info('Currently choices for "LANGUAGE" are: "en", "es", "fr", "it", "pt", "vn"')
            self.lang = '_'                       
        elif self.lang == 'en'  or self.lang == 'es' or self.lang == 'fr' or self.lang == 'it' or self.lang == 'pt' or self.lang=='vn':
            self.logger.info('Language attribute recognized, using "{}" for the facebook interface'.format(self.lang))
        else:
            self.logger.info('Lang "{}" not currently supported'.format(self.lang))                             
            self.logger.info('Currently supported languages are: "en", "es", "fr", "it", "pt" , "vn"')                             
            self.logger.info('Change your interface lang from facebook and try again')
            raise AttributeError('Language provided not currently supported')

        #current year, this variable is needed for parse_page recursion
        self.k = 2019
        #count number of posts, used to prioritized parsing and correctly insert in the csv
        self.count = 0
         
        
        
    def start_requests(self):
        Cookie={
            'sb':'fWWcXEQUgsgsYEkc00Kop0Hh',
            'datr':'Uj7VXK7Hat_PkeLnOHli4IjS',
            'c_user':'100036164642753',
            'xs':'16%3ANbDJvmZLpIPORw%3A2%3A1560314460%3A1270%3A6047',
            'spin':'r.1000874820_b.trunk_t.1561426720_s.1_v.2_',
            'm_pixel_ratio':'1; act=1561427209181%2F8',
            'presence':'EDvF3EtimeF1561427834EuserFA21B36164642753A2EstateFDutF1561427834688CEchFDp_5f1B36164642753F1CC',
            'x-referer':'eyJyIjoiL2hvbWUucGhwIiwiaCI6Ii9ob21lLnBocCIsInMiOiJtIn0%3D',
            'wd':'811x657',
            'fr':'1dt0EEGABt85hujvi.AWWdMXM5IJVHymRWmUl8AFO2_5s.BcnGV9._O.F0R.0.0.BdEY2y.AWXCqnV-'
        }
        urls = ['https://mbasic.facebook.com']   
        #print(Cookie)
        yield scrapy.Request(url=urls[0],
                            cookies=Cookie,
                            callback=self.parse_home
                            )

    #def parse(self, response):

    #    return FormRequest.from_response(
    #       response,
    #        formxpath = '//form[contains(@class,"mobile-login")]',
    #        formdata = {
    #            'email': self.email, 'pass': self.password
    #        }, callback = self.parse_home, meta={'flag':'home'}
    #    )

    def parse_home(self, response):
        '''filename = 'home.html'
        with open(filename, 'wb') as f:
            f.write(response.body)
        self.log('Saved file %s' % filename)'''
        if response.xpath("//div/a[contains(@href,'save-device')]"):
            self.logger.info('Got stuck in "save-device" checkpoint')
            self.logger.info('I will now try to redirect to the correct page')
            return FormRequest.from_response(
                response,
                formdata={'name_action_selected': 'dont_save'},
                callback=self.parse_home
                )

        if self.lang == '_':
            if response.xpath("//input[@placeholder='Search Facebook']"):
                self.logger.info('Language recognized: lang="en"')
                self.lang = 'en'
            elif response.xpath("//input[@placeholder='Buscar en Facebook']"):
                self.logger.info('Language recognized: lang="es"')
                self.lang = 'es'
            elif response.xpath("//input[@placeholder='Rechercher sur Facebook']"):
                self.logger.info('Language recognized: lang="fr"')
                self.lang = 'fr'
            elif response.xpath("//input[@placeholder='Cerca su Facebook']"):
                self.logger.info('Language recognized: lang="it"')
                self.lang = 'it'
            elif response.xpath("//input[@placeholder='Pesquisa no Facebook']"):
                self.logger.info('Language recognized: lang="pt"')
                self.lang = 'pt'
            elif response.xpath("//input[@placeholder='Tìm kiếm trên Facebook']"):
                self.logger.info('Language recognized: lang="pt"')
                self.lang = 'vn'
            else:
                raise AttributeError('Language not recognized\n'
                                     'Change your interface lang from facebook ' 
                                     'and try again')
                                                                 
        #navigate to provided page
        href = response.urljoin(self.page)
        print(href)
        self.logger.info('Scraping facebook page {}'.format(href))
        return scrapy.Request(url=href,callback=self.parse_page,meta={'post':''}) 

    def parse_page(self,response):
        node = GetNeo4j()
        lan = self.lang
        '''filename = 'page.html'
        with open(filename, 'wb') as f:
            f.write(response.body)
        self.log('Saved file %s' % filename)'''
        # i là số lượng thời gian lấy được
        if(response.meta['post']==''):
            i = 0
        else:
            i = int(response.meta['post'])
        datetimes = response.xpath('//abbr/text()').extract()
        if(self.lang=="vn"):
            for dates in datetimes:
                i = i+1
                print('date gốc',dates)
            #if(self.lang=="vn"):
                if(dates.find('201')==-1 and dates.find('200')==-1): #những bài viết trong năm 2019
                    if(dates.find('tháng')==-1): # những bài viết trong năm 2019 nhưng rất gần hiện tại 
                        now = datetime.now()
                        #print(now)
                        time = now.strftime("%d/%m/%Y/%H/%M/%S")
                        #thời gian hiện tại
                        time_list = time.split('/')
                        current_day = int(time_list[0])
                        current_month = int(time_list[1])
                        current_year = int(time_list[2])
                        current_hour = int(time_list[3])
                        current_minute = int(time_list[4])
                    # nếu thời gian tìm được là nhiều giờ trước
                        if(dates.find('giờ')!=-1):
                            hour_list = dates.split(' ')
                            hour = int(hour_list[0])
                            if(hour<=current_hour):
                                current_hour = current_hour-hour
                            else: #nếu giờ lớn hơn giờ hiện tại thì lùi 1 ngày
                            
                                current_hour = 24-(hour-current_hour)
                                if(current_day>1):
                                    current_day = current_day-1
                                else: #nếu ngày trùng với mùng 1 thì lùi 1 tháng
                                    # đối với những tháng có 31 ngày sau khi lùi
                                    if(current_month==2 or current_month==4 or current_month==6 or current_month==8 or current_month==9 or current_month==11 or current_month==1):
                                        if(current_month==1):# nếu là tháng 1 thì lùi 1 năm
                                            current_day = 31
                                            current_month = 12
                                            current_year = current_year-1
                                        else:
                                            current_day=31
                                            current_month = current_month-1
                                    # đối với tháng 2 sau khi lùi có 28 ngày
                                    elif(current_month==3):
                                        current_day = 28
                                        current_month = 2
                                    # còn lại là các tháng có 30 ngày
                                    else:
                                        current_day = 30
                                        current_month = current_month-1
                    # nếu thời gian tìm được là nhiều phút trước
                        elif(dates.find('phút')!=-1):
                            minute_list = dates.split(' ')
                            minute = int(minute_list[0])
                            if(minute<=current_minute):
                                current_minute = current_minute-minute
                            else: # nếu phút lớn hơn phút hiện tại thì lùi 1 giờ
                                current_minute = 60-(minute-current_minute)
                                if(current_hour>0):
                                    current_hour = current_hour-1
                                else: # nếu giờ = 0h thì lùi 1 ngày
                                    current_hour = 23
                                    if(current_day>1):
                                        current_day = current_day-1
                                    else: #nếu ngày trùng với mùng 1 thì lùi 1 tháng
                                        # đối với những tháng có 31 ngày sau khi lùi
                                        if(current_month==2 or current_month==4 or current_month==6 or current_month==8 or current_month==9 or current_month==11 or current_month==1):
                                            if(current_month==1):# nếu là tháng 1 thì lùi 1 năm
                                                current_day = 31
                                                current_month = 12
                                                current_year = current_year-1
                                            else:
                                                current_day=31
                                                current_month = current_month-1
                                        # đối với tháng 2 sau khi lùi có 28 ngày
                                        elif(current_month==3):
                                            current_day = 28
                                            current_month = 2
                                        # còn lại là các tháng có 30 ngày
                                        else:
                                            current_day = 30
                                            current_month = current_month-1
                    #thời gian tìm thấy là hôm qua
                        elif(dates.find("Hôm qua")!=-1):
                            if(dates.find('lúc')!=-1):
                                dates = dates.replace('lúc ','') 
                            if(dates.find(',')!=-1):
                                dates = dates.replace(',','')   
                            dates = dates.replace('Hôm qua','')
                            hour_minute = dates.split(':')
                            current_hour = int(hour_minute[0])
                            current_minute = int(hour_minute[1])
                            if(current_day>1):
                                current_day = current_day-1
                            #nếu ngày trùng với mùng 1 thì lùi 1 tháng
                            else:
                                # đối với những tháng có 31 ngày
                                if(current_month==2 or current_month==4 or current_month==6 or current_month==8 or current_month==9 or current_month==11 or current_month==1):
                                    if(current_month==1): #nếu là tháng 1 thì lùi 1 năm
                                        current_day = 31
                                        current_month = 12
                                        current_year = current_year-1
                                    else:
                                        current_day=31
                                        current_month = current_month-1
                                # đối với tháng 2 sau khi lùi có 28 ngày
                                elif(current_month==3):
                                    current_day = 28
                                    current_month = 2
                                # còn lại là các tháng có 30 ngày
                                else:
                                    current_day = 30
                                    current_month = current_month-1
                        #thời gian tìm thấy có từ 'Hôm nay'
                        elif(dates.find('Hôm nay')!=-1):
                            if(dates.find('lúc')!=-1):
                                dates = dates.replace('lúc ','')
                            if(dates.find(',')!=-1):
                                dates = dates.replace(',','')    
                            dates = dates.replace('Hôm nay','')
                            hour_minute = dates.split(':')
                            current_hour = int(hour_minute[0])
                            current_minute = int(hour_minute[1])
                        # nếu thời gian tìm được có dạng Thứ Hai, Ba... hoặc Chủ Nhật + giờ phút
                        elif(dates.find('Thứ')!=-1 or dates.find('Chủ Nhật')!=-1 ):
                            if(dates.find('Thứ')!=-1):
                                dates = dates.replace('Thứ ','')
                            if(dates.find('lúc')!=-1):
                                dates = dates.replace('lúc','')
                            if(dates.find(',')!=-1):
                                dates = dates.replace(',','')
                            EN_dict={'Monday':2,'Tuesday':3,'Wednesday':4,'Thursday':5,'Friday':6,'Saturday':7,'Sunday':8}
                            VN_dict={'Hai':2,'ba':3,'tư':4,'năm':5,'sáu':6,'bảy':7,'Chủ Nhật':8}
                            day_list=['Hai','ba','tư','năm','sáu','bảy','Chủ Nhật']
                            for day in day_list:
                                if(dates.find(day)!=-1):
                                    dates = dates.replace(day,'') #chỉ để lại giờ và phút
                                    day1 = now.strftime("%A") #lấy tên ngày, ví dụ 26/6/2019 -> Wednesday
                                    index1 = int(EN_dict[day1]) #chỉ số ngày hiện tại
                                    index2 = int(VN_dict[day])  #chỉ số ngày đăng bài hoặc comment
                                    break
                            hour_minute = dates.split(':')
                            current_hour = int(hour_minute[0]) #Giờ cần lấy
                            current_minute = int(hour_minute[1]) #Phút cần lấy
                            #cons là biến chỉ khoảng cách bằng số giữa ngày hiện tại và ngày bài đăng, hoặc comment
                            if(index1<index2):
                                cons = 7 - (index2-index1)
                            else:
                                cons = index1-index2
                            #nếu ngày hiện tại > cons, ví dụ ngày mùng 8
                            if(current_day>cons):
                                current_day = current_day-cons
                            #nếu ngày hiện tại <= cons thì lùi 1 tháng
                            else:
                                #đối với những tháng có 31 ngày gồm tháng 1,3,5,7,8,10,12
                                if(current_month==2 or current_month==4 or current_month==6 or current_month==8 or current_month==9 or current_month==11 or current_month==1):
                                    if(current_month==1): #nếu là tháng 1 thì lùi 1 năm
                                        current_day = 31 - (cons-current_day)
                                        current_month = 12
                                        current_year = current_year-1
                                    else:
                                        current_day=31 - (cons-current_day)
                                        current_month = current_month-1
                                #tháng 2 có 28 ngày
                                elif(current_month==3):
                                    current_day = 28 - (cons-current_day)
                                    current_month = 2
                                #những tháng còn lại có 30 ngày
                                else:
                                    current_day = 30 - (cons-current_day)
                                    current_month = current_month-1
                        date_object = str(datetime(current_year,current_month,current_day,current_hour,current_minute))
                        print('date sau khi xử lý',date_object)
                        name = "post thứ " + str(i)
                        #node.get_datetime_Ofpost(name,date_object)

                    else: #những bài viết trong năm 2019 nhưng khá xa hiện tại
                        dates = dates.replace(' tháng ',',')
                        if(dates.find(':')!=-1): #có giờ phút

                            if(dates.find('lúc')!=-1):
                                dates = dates.replace(' lúc ',',2019,') #thêm chỉ số năm
                                date_object = datetime.strptime(dates, "%d,%m,%Y,%H:%M")
                                print(date_object)
                            elif(dates.find(',')!=-1):
                                dates = dates.replace(', ',',2019,')
                                date_object = datetime.strptime(dates, "%d,%m,%Y,%H:%M")
                                print(date_object)
                            else:
                                dates = dates.replace(' ',',2019,')
                                date_object = datetime.strptime(dates, "%d,%m,%Y,%H:%M")
                                print(date_object)


                        else:# ko có giờ, phút
                            dates = dates.replace(',',',2019,') #thêm chỉ số năm
                            date_object = str(datetime.strptime(dates, "%d,%Y,%m"))
                            print('date sau khi xử lý',date_object)
                        name = "post thứ " + str(i)
                        #node.get_datetime_Ofpost(name,date_object)

                else: #bài viết những năm trước năm 2019
                    if(dates.find(':')!=-1):
                        parts = dates.split(' ')
                        if(parts[0]!='Tháng'):
                            if(dates.find('năm')!=-1):#có từ 'năm'
                                if(dates.find('lúc')!=-1):
                                    dates = dates.replace('lúc ','')
                                if(dates.find(',')!=-1):
                                    dates = dates.replace(',','')
                                dates = dates.replace('năm ','')
                                dates = dates.replace('tháng ','')
                                date_object = str(datetime.strptime(dates,'%d %m %Y %H:%M'))
                                print(date_object)
                            else:
                                if(dates.find('lúc')!=-1):
                                    dates = dates.replace('lúc ','')
                                if(dates.find(',')!=-1):
                                    dates = dates.replace(',','')
                                dates = dates.replace('tháng ','')
                                date_object = str(datetime.strptime(dates,'%d %m %Y %H:%M'))
                                print(date_object)
                        else:
                            if(dates.find('năm')!=-1):#có từ 'năm'
                                if(dates.find('lúc')!=-1):
                                    dates = dates.replace('lúc ','')
                                if(dates.find(',')!=-1):
                                    dates = dates.replace(',','')
                                dates = dates.replace('năm ','')
                                dates = dates.replace('Tháng ','')
                                date_object = str(datetime.strptime(dates,'%m %Y %H:%M'))
                                print(date_object)
                            else:
                                if(dates.find('lúc')!=-1):
                                    dates = dates.replace('lúc ','')
                                if(dates.find(',')!=-1):
                                    dates = dates.replace(',','')
                                dates = dates.replace('Tháng ','')
                                date_object = str(datetime.strptime(dates,'%m %Y %H:%M'))
                                print(date_object)
                    else:
                        parts = dates.split(' ')
                        if(parts[0]!='Tháng'):
                            if(len(parts)<=2):
                                if(dates.find('Năm')!=-1):
                                    dates = dates.replace('Năm ','')
                                    date_object = str(datetime.strptime(dates,'%Y'))
                                    print(date_object)
                                else:
                                    date_object = str(datetime.strptime(dates,'%Y'))
                                    print(date_object)
                            else:
                                if(dates.find('năm')!=-1):#có từ 'năm'
                                    if(dates.find(',')!=-1):
                                        dates = dates.replace(',','')
                                    dates = dates.replace('năm ','')
                                    dates = dates.replace('tháng ','')
                                    date_object = str(datetime.strptime(dates,'%d %m %Y'))
                                    print(date_object)
                                else:
                                    if(dates.find(',')!=-1):
                                        dates = dates.replace(',','')
                                    dates = dates.replace('tháng ','')
                                    date_object = str(datetime.strptime(dates,'%d %m %Y'))
                                    print(date_object)
                        else:
                            if(dates.find('năm')!=-1):#có từ 'năm'
                                if(dates.find(',')!=-1):
                                    dates = dates.replace(',','')
                                dates = dates.replace('năm ','')
                                dates = dates.replace('Tháng ','')
                                date_object = str(datetime.strptime(dates,'%m %Y'))
                                print(date_object)
                            else:
                                if(dates.find(',')!=-1):
                                    dates = dates.replace(',','')
                                dates = dates.replace('Tháng ','')
                                date_object = str(datetime.strptime(dates,'%m %Y'))
                                print(date_object)

                    #if(dates.find('lúc')==-1):
                    #    if(dates.find('tháng')!=-1):
                    #        dates = dates.replace(' tháng ',',')
                    #        date_object = str(datetime.strptime(dates,'%d,%m, %Y'))
                    #        print('date sau khi xử lý',date_object)
                    #    elif(dates.find('Tháng')!=-1):
                    #        dates = dates.replace('Tháng ','')
                    #        if(dates.find('năm')!=-1):
                    #           dates = dates.replace(' năm ',',')
                    #            print('date là',dates)
                    #            date_object = str(datetime.strptime(dates,'%m,%Y'))
                    #            print('date sau khi xử lý',date_object)
                    #        else:
                                #print(dates)
                    #            date_object = str(datetime.strptime(dates,'%m,%Y'))
                    #            print('date sau khi xử lý',date_object)
                    #    else:
                    #        if(dates.find('Năm')!=-1):
                    #            dates = dates.replace('Năm ','')
                    #            date_object = str(datetime.strptime(dates,'%Y'))
                    #            print('date sau khi xử lý',date_object)
                    #       else:
                    #            print('date là', dates)
                    #            date_object = str(datetime.strptime(dates,'%Y'))
                    #            print('date sau khi xử lý',date_object)
                        
                    #else:
                    #    dates = dates.replace(' tháng ',',')
                    #    dates = dates.replace(' lúc ',',')
                    #    date_object = str(datetime.strptime(dates,'%d,%m, %Y,%H:%M'))
                    #    print('date sau khi xử lý',date_object)
                    #print(date)
                    #date_object = str(datetime.strptime(date,'%d,%m, %Y,%H:%M'))
                    #print('date sau khi xử lý',date_object)
                    #name = "post thứ " + str(i)
                    #node.get_datetime_Ofpost(name,date_object)
        elif(self.lang =="en"):
            for dates in datetimes:
                i = i+1
                print('date gốc',dates)
            #elif(self.lang=='en'):
                #if(dates.find('Yesterday')!=-1):
                #    dates = dates.replace('Yesterday','Hôm qua')
                #    #print(dates)
                #    if(dates.find('at')!=-1):
                #        dates = dates.replace('at', 'lúc')
                        #print(dates)
                #a1 = dates.find('hrs')
                #a2 = dates.find('hour')
                #a3 = dates.find('hours')
                #a4 = dates.find('mins')
                #a5 = dates.find('minute')
                #a6 = dates.find('minutes')
                
                    
                #if(a1!=-1 or a2!=-1 or a3!=-1 or a4!=-1 or a5!=-1 or a6!=-1):
                #    list1 = ['hrs','hour','hours','mins','minute','minutes']
                #    dict1 = {'hrs':'giờ','hour':'giờ','hours':'giờ','mins':'phút','minute':'phút','minutes':'phút'}
                #    for hm in list1:
                #        if(dates.find(hm)!=-1):
                #            dates = dates.replace(hm,dict1[hm])
                #            #print(dates)
                #month_dict={
                #    'January ': 'Tháng 1','February ':'Tháng 2','March ':'Tháng 3','April ':'Tháng 4',
                #    'May ':'Tháng 5','June ':'Tháng 6', 'July ':'Tháng 7','August ':'Tháng 8',
                #    'September ':'Tháng 9','October ':'Tháng 10','November ':'Tháng 11','December ':'Tháng 12'
                #}
                #month_list=['January ','February ','March ','April ','May ','June ','July ','August ','September ','October ','November ','December ']
                #EN_dict={'Monday':2,'Tuesday':3,'Wednesday':4,'Thursday':5,'Friday':6,'Saturday':7,'Sunday':8}
                #for month in month_list:
                #    if(dates.find(month)!=-1):
                #        dates = dates.replace(month,'')
                #        if(dates.find('at')!=-1 and dates.find(',')==-1):
                #            str1 = month_dict[month] + " lúc"
                #            dates = dates.replace('at',str1)
                #            #print(dates)
                #        if(dates.find('at')!=-1 and dates.find(',')!=-1):
                #            str1 = " "+month_dict[month] +","
                #            dates = dates.replace(',',str1)
                #            dates = dates.replace('at','lúc')
                            #print(dates)
                if(dates.find('201')==-1 and dates.find('200')==-1): #những bài viết trong năm 2019
                    month_list=['January','February','March','April','May','June','July','August','September','October','November','December',
                                'Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
                    index_month = 0
                    month_finded = ""
                    for month in month_list:
                        if(dates.find(month)!=-1):
                            index_month = 1
                            month_finded = month
                            break
                    if(index_month == 0): # những bài viết trong năm 2019 nhưng rất gần hiện tại 
                        now = datetime.now()
                        #print(now)
                        time = now.strftime("%d/%m/%Y/%H/%M/%S")
                        #thời gian hiện tại
                        time_list = time.split('/')
                        current_day = int(time_list[0])
                        current_month = int(time_list[1])
                        current_year = int(time_list[2])
                        current_hour = int(time_list[3])
                        current_minute = int(time_list[4])
                    
                        list1 = ['hrs','hour','hours']
                        list2 = ['mins','minute','minutes']
                        Day_list = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday']
                        index_day = 0
                        index_hour = 0
                        index_minute = 0
                        for hour in list1:
                            if(dates.find(hour)!=-1):
                                index_hour = index_hour + 1
                                break
                        for minute in list2:
                            if(dates.find(minute)!=-1):
                                index_minute = index_minute + 1
                        
                        for day in Day_list:
                            if(dates.find(day)!=-1):
                                index_day = index_day + 1
                                day_finded = day
                                break
                        if(index_hour!=0): # thời gian là giờ trước
                            hour_list = dates.split(' ')
                            hour = int(hour_list[0])
                            if(hour<=current_hour):
                                current_hour = current_hour-hour
                            else: #nếu giờ lớn hơn giờ hiện tại thì lùi 1 ngày
                            
                                current_hour = 24-(hour-current_hour)
                                if(current_day>1):
                                    current_day = current_day-1
                                else: #nếu ngày trùng với mùng 1 thì lùi 1 tháng
                                    # đối với những tháng có 31 ngày sau khi lùi
                                    if(current_month==2 or current_month==4 or current_month==6 or current_month==8 or current_month==9 or current_month==11 or current_month==1):
                                        if(current_month==1):# nếu là tháng 1 thì lùi 1 năm
                                            current_day = 31
                                            current_month = 12
                                            current_year = current_year-1
                                        else:
                                            current_day=31
                                            current_month = current_month-1
                                    # đối với tháng 2 sau khi lùi có 28 ngày
                                    elif(current_month==3):
                                        current_day = 28
                                        current_month = 2
                                    # còn lại là các tháng có 30 ngày
                                    else:
                                        current_day = 30
                                        current_month = current_month-1
                    # nếu thời gian tìm được là nhiều phút trước
                        elif(index_minute!=0):
                            minute_list = dates.split(' ')
                            minute = int(minute_list[0])
                            if(minute<=current_minute):
                                current_minute = current_minute-minute
                            else: # nếu phút lớn hơn phút hiện tại thì lùi 1 giờ
                                current_minute = 60-(minute-current_minute)
                                if(current_hour>0):
                                    current_hour = current_hour-1
                                else: # nếu giờ = 0h thì lùi 1 ngày
                                    current_hour = 23
                                    if(current_day>1):
                                        current_day = current_day-1
                                    else: #nếu ngày trùng với mùng 1 thì lùi 1 tháng
                                        # đối với những tháng có 31 ngày sau khi lùi
                                        if(current_month==2 or current_month==4 or current_month==6 or current_month==8 or current_month==9 or current_month==11 or current_month==1):
                                            if(current_month==1):# nếu là tháng 1 thì lùi 1 năm
                                                current_day = 31
                                                current_month = 12
                                                current_year = current_year-1
                                            else:
                                                current_day=31
                                                current_month = current_month-1
                                        # đối với tháng 2 sau khi lùi có 28 ngày
                                        elif(current_month==3):
                                            current_day = 28
                                            current_month = 2
                                        # còn lại là các tháng có 30 ngày
                                        else:
                                            current_day = 30
                                            current_month = current_month-1
                    #thời gian tìm thấy là hôm qua (Yesterday)
                        elif(dates.find("Yesterday")!=-1):
                            if(dates.find('at')!=-1):
                                dates = dates.replace('at','')
                            if(dates.find(',')!=-1):
                                dates = dates.replace(',','')    
                            dates = dates.replace('Yesterday','')
                            if(dates.find('PM')!=-1):
                                dates = dates.replace('PM','')
                                hour_minute = dates.split(':')
                                if(int(hour_minute[0])==12):
                                    current_hour = int(hour_minute[0]) 
                                else:
                                    current_hour = int(hour_minute[0]) + 12
                                current_minute = int(hour_minute[1])
                            elif(dates.find('AM')!=-1):
                                dates = dates.replace('AM','')
                                hour_minute = dates.split(':')
                                current_hour = int(hour_minute[0])
                                current_minute = int(hour_minute[1])
                            else:
                                hour_minute = dates.split(':')
                                current_hour = int(hour_minute[0])
                                current_minute = int(hour_minute[1])
                            if(current_day>1):
                                current_day = current_day-1
                            #nếu ngày trùng với mùng 1 thì lùi 1 tháng
                            else:
                                # đối với những tháng có 31 ngày
                                if(current_month==2 or current_month==4 or current_month==6 or current_month==8 or current_month==9 or current_month==11 or current_month==1):
                                    if(current_month==1): #nếu là tháng 1 thì lùi 1 năm
                                        current_day = 31
                                        current_month = 12
                                        current_year = current_year-1
                                    else:
                                        current_day=31
                                        current_month = current_month-1
                                # đối với tháng 2 sau khi lùi có 28 ngày
                                elif(current_month==3):
                                    current_day = 28
                                    current_month = 2
                                # còn lại là các tháng có 30 ngày
                                else:
                                    current_day = 30
                                    current_month = current_month-1
                        # nếu thời gian tìm được có từ Today
                        elif(dates.find('Today')!=-1):
                            if(dates.find('at')!=-1):
                                dates = dates.replace('at','')
                            if(dates.find(',')!=-1):
                                dates = dates.replace(',','')    
                            dates = dates.replace('Today','')
                            if(dates.find('PM')!=-1):
                                dates = dates.replace('PM','')
                                hour_minute = dates.split(':')
                                if(int(hour_minute[0])==12):
                                    current_hour = int(hour_minute[0])
                                else:
                                    current_hour = int(hour_minute[0]) + 12
                                current_minute = int(hour_minute[1])
                            elif(dates.find('AM')!=-1):
                                dates = dates.replace('AM','')
                                hour_minute = dates.split(':')
                                current_hour = int(hour_minute[0])
                                current_minute = int(hour_minute[1])
                            else:
                                hour_minute = dates.split(':')
                                current_hour = int(hour_minute[0])
                                current_minute = int(hour_minute[1])
                        # nếu thời gian tìm được có dạng Monday, Tuesday... Sunday + giờ phút
                        
                            
                        elif(index_day!=0):
                            if(dates.find('at')!=-1):
                                dates = dates.replace('at','')
                            if(dates.find(',')!=-1):
                                dates = dates.replace(',','')
                            EN_dict={'Monday':2,'Tuesday':3,'Wednesday':4,'Thursday':5,'Friday':6,'Saturday':7,'Sunday':8}
                            
                            #VN_dict={'Hai':2,'ba':3,'tư':4,'năm':5,'sáu':6,'bảy':7,'Chủ Nhật':8}
                            #day_list=['Hai','ba','tư','năm','sáu','bảy','Chủ Nhật']
                            
                                
                            dates = dates.replace(day_finded,'') #chỉ để lại giờ và phút
                            day1 = now.strftime("%A") #lấy tên ngày, ví dụ 26/6/2019 -> Wednesday
                            index1 = int(EN_dict[day1])  #chỉ số ngày hiện tại
                            index2 = int(EN_dict[day_finded])  #chỉ số ngày đăng bài hoặc comment
                                
                            if(dates.find('PM')!=-1):
                                dates = dates.replace('PM','')
                                hour_minute = dates.split(':')
                                if(int(hour_minute[0])==12):
                                    current_hour = int(hour_minute[0])
                                else:
                                    current_hour = int(hour_minute[0]) + 12
                                current_minute = int(hour_minute[1])
                            elif(dates.find('AM')!=-1):
                                dates = dates.replace('AM','')
                                hour_minute = dates.split(':')
                                current_hour = int(hour_minute[0])
                                current_minute = int(hour_minute[1])
                            else:
                                hour_minute = dates.split(':')
                                current_hour = int(hour_minute[0])
                                current_minute = int(hour_minute[1])
                            #hour_minute = dates.split(':')
                            #current_hour = int(hour_minute[0]) #Giờ cần lấy
                            #current_minute = int(hour_minute[1]) #Phút cần lấy
                            #cons là biến chỉ khoảng cách bằng số giữa ngày hiện tại và ngày bài đăng, hoặc comment
                            if(index1<index2):
                                cons = 7 - (index2-index1)
                            else:
                                cons = index1-index2
                            #nếu ngày hiện tại > cons, ví dụ ngày mùng 8
                            if(current_day>cons):
                                current_day = current_day-cons
                            #nếu ngày hiện tại <= cons thì lùi 1 tháng
                            else:
                                #đối với những tháng có 31 ngày gồm tháng 1,3,5,7,8,10,12
                                if(current_month==2 or current_month==4 or current_month==6 or current_month==8 or current_month==9 or current_month==11 or current_month==1):
                                    if(current_month==1): #nếu là tháng 1 thì lùi 1 năm
                                        current_day = 31 - (cons-current_day)
                                        current_month = 12
                                        current_year = current_year-1
                                    else:
                                        current_day=31 - (cons-current_day)
                                        current_month = current_month-1
                                #tháng 2 có 28 ngày
                                elif(current_month==3):
                                    current_day = 28 - (cons-current_day)
                                    current_month = 2
                                #những tháng còn lại có 30 ngày
                                else:
                                    current_day = 30 - (cons-current_day)
                                    current_month = current_month-1
                        date_object = str(datetime(current_year,current_month,current_day,current_hour,current_minute))
                        print('date sau khi xử lý',date_object)
                        name = "post thứ " + str(i)
                        #node.get_datetime_Ofpost(name,date_object)

                    else: #những bài viết trong năm 2019 nhưng khá xa hiện tại
                        month_dict={'January':'1','February':'2','March':'3','April':'4','May':'5','June':'6','July':'7',
                                    'August':'8','September':'9','October':'10','November':'11','December':'12',
                                    'Jan':'1','Feb':'2','Mar':'3','Apr':'4','Jun':'6','Jul':'7','Aug':'8','Sep':'9','Oct':'10','Nov':'11','Dec':'12'}
                        parts = dates.split(' ')
                        if(month_finded==parts[0]): #tháng ở đầu
                            dates = dates.replace(month_finded,month_dict[month_finded])
                            if(dates.find(':')==-1):
                                month_day = dates.split(' ')
                                current_month = int(month_day[0])
                                current_day = int(month_day[1])
                                current_year = 2019
                                date_object = str(datetime(current_year,current_month,current_day))
                                print(date_object)
                            else:

                                if(dates.find('at')!=-1):
                                    dates = dates.replace('at ','')
                                if(dates.find(',')!=-1):
                                    dates = dates.replace(',','')
                                if(dates.find('PM')!=-1):
                                    dates = dates.replace(' PM','')
                                    time_finded = dates.split(' ')
                                    current_month = int(time_finded[0])
                                    current_day = int(time_finded[1])
                                    hour_mins = time_finded[2].split(':')
                                    if(int(hour_mins[0])==12):
                                        current_hour = int(hour_mins[0])
                                    else:
                                        current_hour = int(hour_mins[0]) + 12
                                    current_minute = int(hour_mins[1])
                                elif(dates.find('AM')!=-1):
                                    dates = dates.replace(' AM','')
                                    time_finded = dates.split(' ')
                                    current_month = int(time_finded[0])
                                    current_day = int(time_finded[1])
                                    hour_mins = time_finded[2].split(':')
                                    current_hour = int(hour_mins[0])
                                    current_minute = int(hour_mins[1])
                                else:
                                    time_finded = dates.split(' ')
                                    current_month = int(time_finded[0])
                                    current_day = int(time_finded[1])
                                    hour_mins = time_finded[2].split(':')
                                    current_hour = int(hour_mins[0])
                                    current_minute = int(hour_mins[1])
                                current_year = 2019
                                date_object = str(datetime(current_year,current_month,current_day,current_hour,current_minute))
                                print(date_object)

                            
                        else: #tháng ở vị trí thứ 2 
                            dates = dates.replace(month_finded,int(month_dict[month_finded]))
                            if(dates.find(':')==-1):
                                month_day = dates.split(' ')
                                current_month = int(month_day[1])
                                current_day = int(month_day[0])
                                current_year = 2019
                                date_object = str(datetime(current_year,current_month,current_day))
                                print(date_object)
                            else:

                                if(dates.find('at')!=-1):
                                    dates = dates.replace('at ','')
                                if(dates.find(',')!=-1):
                                    dates = dates.replace(',','')
                                if(dates.find('PM')!=-1):
                                    dates = dates.replace(' PM','')
                                    time_finded = dates.split(' ')
                                    current_month = int(time_finded[1])
                                    current_day = int(time_finded[0])
                                    hour_mins = time_finded[2].split(':')
                                    if(int(hour_mins[0])==12):
                                        current_hour = int(hour_mins[0])
                                    else:
                                        current_hour = int(hour_mins[0]) + 12
                                    current_minute = int(hour_mins[1])
                                elif(dates.find('AM')!=-1):
                                    dates = dates.replace(' AM','')
                                    time_finded = dates.split(' ')
                                    current_month = int(time_finded[1])
                                    current_day = int(time_finded[0])
                                    hour_mins = time_finded[2].split(':')
                                    current_hour = int(hour_mins[0])
                                    current_minute = int(hour_mins[1])
                                else:
                                    time_finded = dates.split(' ')
                                    current_month = int(time_finded[1])
                                    current_day = int(time_finded[0])
                                    hour_mins = time_finded[2].split(':')
                                    current_hour = int(hour_mins[0])
                                    current_minute = int(hour_mins[1])
                                current_year = 2019
                                date_object = str(datetime(current_year,current_month,current_day,current_hour,current_minute))
                                print(date_object)
                        #node.get_datetime_Ofpost(name,date_object)

                else: #bài viết những năm trước năm 2019, thiếu TH tháng đứng sau ngày
                    month_list1=['January','February','March','April','May','June','July','August','September','October','November','December']
                    month_list2=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
                    index_month = 0
                    month_finded = ""
                    for month in month_list1:
                        if(dates.find(month)!=-1):
                            index_month = 1
                            break
                        else:
                            for month in month_list2:
                                if(dates.find(month)!=-1):
                                    index_month = 2
                                    break
                    if(dates.find(':')!=-1):
                        if(dates.find('at')!=-1):
                            dates = dates.replace('at ','')
                        if(dates.find(',')!=-1):
                            dates = dates.replace(',','')
                        if(dates.find('AM')!=-1):
                            dates = dates.replace(' AM','')
                            if(index_month==1):
                                date_object = str(datetime.strptime(dates,'%B %d %Y %H:%M'))
                            elif(index_month==2):
                                date_object = str(datetime.strptime(dates,'%b %d %Y %H:%M'))
                            print('date sau khi xử lý',date_object)
                        elif(dates.find('PM')!=-1):
                            dates = dates.replace(' PM','')
                            if(index_month==1):
                                date_object = datetime.strptime(dates,'%B %d %Y %H:%M')
                            elif(index_month==2):
                                date_object = datetime.strptime(dates,'%b %d %Y %H:%M')
                            date_str = date_object.strftime("%d/%m/%Y/%H/%M/%S")
                            lst = date_str.split('/')
                            current_day = int(lst[0])
                            current_month = int(lst[1])
                            current_year = int(lst[2])
                            if(int(lst[3])==12):
                                current_hour = int(lst[3])
                            else:
                                current_hour = int(lst[3]) + 12
                            current_minute = int(lst[4])
                            date_object = str(datetime(current_year,current_month,current_day,current_hour,current_minute))
                            print(date_object)
                        else:
                            if(index_month==1):
                                date_object = str(datetime.strptime(dates,'%B %d %Y %H:%M'))
                            elif(index_month==2):
                                date_object = str(datetime.strptime(dates,'%b %d %Y %H:%M'))
                            print('date sau khi xử lý',date_object)
                    else: # không có giờ
                        if(dates.find(',')!=-1):
                            dates = dates.replace(',','')
                        datetime_list = dates.split(' ')
                        
                        if(len(datetime_list)==3):
                            if(index_month==1):
                                date_object = datetime.strptime(dates,'%B %d %Y')
                            elif(index_month==2):
                                date_object = datetime.strptime(dates,'%b %d %Y')
                        elif(len(datetime_list)==2):
                            if(index_month==1):
                                date_object = datetime.strptime(dates,'%B %Y')
                            elif(index_month==2):
                                date_object = datetime.strptime(dates,'%b %Y')
                        else:
                            date_object = datetime.strptime(dates,'%Y')
                        print(date_object)
                        
                    

            
                                


            #Click nút seemore
            # url see_more dành cho bài comment
        #url = response.xpath('//div[contains(@id,"see_next")]/a/@href').extract_first() 
            # url see_more dành cho các bài đăng
        url = response.xpath('//*[@id="structured_composer_async_container"]/div[2]/a[@href][not(contains(@href,"end"))]/@href').extract_first()
        if(url==None):
            print("Not See More")
            print(i)
        else:
            #print(i)
            url = "https://mbasic.facebook.com"+url   
            yield scrapy.Request(url=url, callback=self.parse_page, meta={'post':i})
    

            

